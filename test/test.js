const request = require("supertest");
const jwt = require("jsonwebtoken");
const jwt_decode = require("jwt-decode");
const app = require("../index");
const {
  chapter,
  report,
  student,
  subject,
  teacher,
  sequelize,
} = require("../db/models");

//////////////////////////////////////////////////////////////// Student Sign Up / Sign In //////////////////////////////////////////////////////////////////
// Student Auth test
// Student Sign Up
describe("Auth Test Student", () => {
  describe("/student/signup POST", () => {
    it("It should make user and get the token", async () => {
      const res = await request(app).post("/student/signup").send({
        name: "Alfian Yuandika",
        username: "alfian1234",
        grade: 1,
        password: "Alfian_1234",
        confirmPassword: "Alfian_1234",
      });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
      expect(res.body).toHaveProperty("token");
    });

    it("It should error username has been used", async () => {
      const res = await request(app).post("/student/signup").send({
        name: "Alfian Yuandika",
        username: "alfian1234",
        grade: 1,
        password: "Alfian_1234",
        confirmPassword: "Alfian_1234",
      });
      expect(res.statusCode).toEqual(401);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Username has been used");
    });

    it("It should error Grade must be between 0 - 12 ", async () => {
      const res = await request(app).post("/student/signup").send({
        name: "Alfian Yuandika",
        username: "alfian1234",
        grade: 15,
        password: "Alfian_1234",
        confirmPassword: "Alfian_1234",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Grade must be between 0 - 12 ");
    });

    it("It should error password is not strong enough ", async () => {
      const res = await request(app).post("/student/signup").send({
        name: "Alfian Yuandika",
        username: "alfian1234",
        grade: 1,
        password: "Alfian",
        confirmPassword: "Alfian",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual(
        "Password must have minimum length of 8 characters with minimum 1 lowercase character, 1 uppercase character, 1 number and 1 symbol"
      );
    });

    it("It should error Password confirmation is not the same as password", async () => {
      const res = await request(app).post("/student/signup").send({
        name: "Alfian Putra",
        username: "alfian123456",
        grade: 1,
        password: "Alfian_1234",
        confirmPassword: "Alfian_12345",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual(
        "Password confirmation must be the same with password"
      );
    });

    // Student Sign in
    describe("/student/signin POST", () => {
      it("It should success login", async () => {
        const res = await request(app).post("/student/signin").send({
          username: "alfian1234",
          password: "Alfian_1234",
        });

        expect(res.statusCode).toEqual(200);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
        expect(res.body).toHaveProperty("token");
      });
      it("It should error username not found", async () => {
        const res = await request(app).post("/student/signin").send({
          username: "alfian12345",
          password: "Alfian_1234",
        });

        expect(res.statusCode).toEqual(401);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Username not found");
      });
      it("It should error wrong password", async () => {
        const res = await request(app).post("/student/signin").send({
          username: "alfian1234",
          password: "Alfian_123456",
        });

        expect(res.statusCode).toEqual(401);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Wrong password");
      });
    });
  });
});

//////////////////////////////////////////////////////////////// Teacher Sign Up / Sign In //////////////////////////////////////////////////////////////////
// Teacher Auth test
// Teacher Sign Up
describe("Auth Test Teacher", () => {
  describe("/teacher/signup POST", () => {
    it("It should make user and get the token", async () => {
      const res = await request(app).post("/teacher/signup").send({
        name: "Ahmad Putra",
        username: "ahmad1234",
        password: "Ahmad_1234",
        confirmPassword: "Ahmad_1234",
      });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
      expect(res.body).toHaveProperty("token");
    });

    it("It should error username has been used", async () => {
      const res = await request(app).post("/teacher/signup").send({
        name: "Ahmad Putra",
        username: "ahmad1234",
        password: "Ahmad_1234",
        confirmPassword: "Ahmad_1234",
      });
      expect(res.statusCode).toEqual(401);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Username has been used");
    });

    it("It should error password is not strong enough ", async () => {
      const res = await request(app).post("/teacher/signup").send({
        name: "Ahmad Putra",
        username: "ahmad1234",
        password: "Ahmad",
        confirmPassword: "Ahmad",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual(
        "Password must have minimum length of 8 characters with minimum 1 lowercase character, 1 uppercase character, 1 number and 1 symbol"
      );
    });

    it("It should error Password confirmation is not the same as password", async () => {
      const res = await request(app).post("/teacher/signup").send({
        name: "Ahmad Putra Alfian",
        username: "ahmad123",
        password: "Ahmad_12345",
        confirmPassword: "Ahmad_1234",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual(
        "Password confirmation must be the same with password"
      );
    });

    // Student Sign in
    describe("/teacher/signin POST", () => {
      it("It should success login", async () => {
        const res = await request(app).post("/teacher/signin").send({
          username: "ahmad1234",
          password: "Ahmad_1234",
        });

        expect(res.statusCode).toEqual(200);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
        expect(res.body).toHaveProperty("token");
      });
      it("It should error username not found", async () => {
        const res = await request(app).post("/teacher/signin").send({
          username: "ahmad12345",
          password: "Ahmad_1234",
        });

        expect(res.statusCode).toEqual(401);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Username not found");
      });
      it("It should error wrong password", async () => {
        const res = await request(app).post("/teacher/signin").send({
          username: "ahmad1234",
          password: "Ahmad_123456",
        });

        expect(res.statusCode).toEqual(401);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Wrong password");
      });
    });
  });
});

//////////////////////////////////////////////////////////////// CRUD Subject //////////////////////////////////////////////////////////////////
// Create Subject
describe("Create Subject Test", () => {
  describe("subject/teacher/create POST", () => {
    it("It should success", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).post("/subject/teacher/create/").send({
          id: "English 501",
          subject_name: "English",
          grade: 4,
        });

        expect(res.statusCode).toEqual(200);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      }
    });
    it("It should error grade must be between 1 - 12 ", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).post("/subject/teacher/create/").send({
          id: "English 601",
          subject_name: "English",
          grade: 15,
        });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Grade must be between 1 - 12 ");
      }
    });
    it("It should error not authorized ", async () => {
      if (teacher.role == "student") {
        const res = await request(app).post("/subject/teacher/create/").send({
          id: "English 601",
          subject_name: "English",
          grade: 6,
        });

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
});

// Get One Subject
describe("Get One Subject Test", () => {
  describe("/subject GET", () => {
    it("It should success", async () => {
      const res = await request(app).get("/subject").send({
        subject_id: "English 101",
      });
      expect(res.statusCode).toEqual(201);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
    });
    it("It should error subject not found", async () => {
      const res = await request(app).get("/subject").send({
        subject_id: "English 701",
      });
      expect(res.statusCode).toEqual(404);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Subject Not Found");
    });
  });
});

// Get all Subject
describe("Get One Subject Test", () => {
  describe("/subject GET", () => {
    it("It should success", async () => {
      const res = await request(app).get("/subject/all");
      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
    });
  });
});

//Update Subject
describe("Update Produk Test", () => {
  describe("subject/teacher/update/ PATCH", () => {
    it("It should success", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).patch("/subject/teacher/update").send({
          subject_id: "English 101",
          new_subject_id: "Science 401",
          new_subject_name: "Science",
          new_grade: 4,
        });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Update Subject Success");
      }
    });
    it("It should error grade must be between 1 - 12 ", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).patch("/subject/teacher/update").send({
          subject_id: "English 201",
          new_subject_id: "Science 501",
          new_subject_name: "Science",
          new_grade: 14,
        });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Grade must be between 1 - 12 ");
      }
    });
    it("It should error subject ID not found ", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).patch("/subject/teacher/update").send({
          subject_id: "English 901",
          new_subject_id: "Science 501",
          new_subject_name: "Science",
          new_grade: 1,
        });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Subject ID Not Found");
      }
    });
    it("It should error not authorized", async () => {
      if (teacher.role == "student") {
        const res = await request(app).patch("/subject/teacher/update").send({
          subject_id: "English 901",
          new_subject_id: "Science 501",
          new_subject_name: "Science",
          new_grade: 1,
        });

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
});

//Delete Subject
describe("Delete Produk Test", () => {
  describe("subject/teacher/delete DELETE", () => {
    it("It should success", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).delete("/subject/teacher/delete").send({
          subject_id: "English 101",
        });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Delete Subject Success");
      }
    });
    it("It should error Subject ID Not Found", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).delete("/subject/teacher/delete").send({
          subject_id: "English 801",
        });

        expect(res.statusCode).toEqual(404);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Subject ID Not Found");
      }
    });
    it("It should error not authorized", async () => {
      if (teacher.role == "student") {
        const res = await request(app).delete("/subject/teacher/delete").send({
          subject_id: "English 201",
        });

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
});

//////////////////////////////////////////////////////////////// CRUD Chapter //////////////////////////////////////////////////////////////////
// Create Chapter
describe("Create Chapter Test", () => {
  describe("chapter/teacher/create POST", () => {
    it("It should success", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).post("/chapter/teacher/create/").send({
          chapter_id: "English Advance Bab 4",
          chapter_name: "English Advance 4",
          subject_id: "English 401",
        });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      }
    });
    it("It should error Subject ID Not Found ", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).post("/chapter/teacher/create/").send({
          chapter_id: "English Advance Bab 4",
          chapter_name: "English Advance 4",
          subject_id: "English 901",
        });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Subject ID Not Found");
      }
    });
    it("It should error not authorized ", async () => {
      if (teacher.role == "student") {
        const res = await request(app).post("/chapter/teacher/create/").send({
          chapter_id: "English Advance Bab 4",
          chapter_name: "English Advance 4",
          subject_id: "English 401",
        });

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
});

// Get One Chapter
describe("Get One Chapter Test", () => {
  describe("/chapter GET", () => {
    it("It should success", async () => {
      const res = await request(app).get("/chapter").send({
        chapter_id: "English Advance Bab 1",
      });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
    });
    it("It should error chapter not found", async () => {
      const res = await request(app).get("/chapter").send({
        chapter_id: "English Advance Bab 15",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Chapter not found");
    });
  });
});

// Get all Chapter
describe("Get One Chapter Test", () => {
  describe("/chapter GET", () => {
    it("It should success", async () => {
      const res = await request(app).get("/chapter/all");
      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
    });
  });
});

//Update Chapter
describe("Update Produk Test", () => {
  describe("chapter/teacher/update/ PATCH", () => {
    it("It should success", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).patch("/chapter/teacher/update").send({
          chapter_id: "Indonesian Advance Bab 2",
          new_chapter_id: "Indonesian Advance Bab 4",
          new_chapter_name: "Indonesian",
          new_subject_id: "Indonesian 301",
        });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Update Category Success");
      }
    });
    it("It should error Chapter ID Not Found", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).patch("/chapter/teacher/update").send({
          chapter_id: "Indonesian Advance Bab 20",
          new_chapter_id: "Indonesian Advance Bab 4",
          new_chapter_name: "Indonesian",
          new_subject_id: "Indonesian 301",
        });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Chapter ID Not Found");
      }
    });
    it("It should error Subject ID Not Found ", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).patch("/chapter/teacher/update").send({
          chapter_id: "Indonesian Advance Bab 2",
          new_chapter_id: "Indonesian Advance Bab 4",
          new_chapter_name: "Indonesian",
          new_subject_id: "Indonesian 601",
        });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Subject ID Not Found");
      }
    });
    it("It should error not authorized", async () => {
      if (teacher.role == "student") {
        const res = await request(app).patch("/chapter/teacher/update").send({
          chapter_id: "Indonesian Advance Bab 1",
          new_chapter_id: "Indonesian Advance Bab 4",
          new_chapter_name: "Indonesian",
          new_subject_id: "Indonesian 301",
        });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
});

//Delete Chapter
describe("Delete Produk Test", () => {
  describe("chapter/teacher/delete DELETE", () => {
    it("It should success", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).delete("/chapter/teacher/delete").send({
          chapter_id: "Indonesian Intro Bab 1",
        });

        expect(res.statusCode).toEqual(200);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      }
    });
    it("It should error Chapter ID Not Found", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).delete("/chapter/teacher/delete").send({
          chapter_id: "Indonesian Intro Bab 15",
        });

        expect(res.statusCode).toEqual(404);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Chapter ID Not Found");
      }
    });
    it("It should error not authorized", async () => {
      if (teacher.role == "student") {
        const res = await request(app).delete("/chapter/teacher/delete").send({
          chapter_id: "Indonesian Intro Bab 2",
        });

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
});

//////////////////////////////////////////////////////////////// CRUD Report //////////////////////////////////////////////////////////////////
// Create Report
describe("Create Report Test", () => {
  describe("report/teacher/create POST", () => {
    it("It should success", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).post("/report/teacher/create/").send({
          student_id: "452f6aae-6688-42c3-b576-859e9c0c3a32",
          chapter_id: "English Intro Bab 2",
          score: 75,
          grade: 1,
        });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      }
    });
    it("It should error Student ID Not Found ", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).post("/report/teacher/create/").send({
          student_id: "452f6aae-6688-42c3-b576-859e9c0c3a3",
          chapter_id: "English Intro Bab 2",
          score: 75,
          grade: 1,
        });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Student ID Not Found");
      }
    });
    it("It should error Chapter ID Not Found ", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).post("/report/teacher/create/").send({
          student_id: "452f6aae-6688-42c3-b576-859e9c0c3a32",
          chapter_id: "English Intro Bab 20",
          score: 75,
          grade: 1,
        });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Chapter ID Not Found");
      }
    });
    it("It should error Score must be between 0 - 100 ", async () => {
      if (teacher.role == "teacher") {
        const res = await request(app).post("/report/teacher/create/").send({
          student_id: "452f6aae-6688-42c3-b576-859e9c0c3a32",
          chapter_id: "English Intro Bab 2",
          score: 750,
          grade: 1,
        });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Score must be between 0 - 100 ");
      }
    });
    it("It should error not authorized ", async () => {
      if (teacher.role == "student") {
        const res = await request(app).post("/report/teacher/create/").send({
          student_id: "452f6aae-6688-42c3-b576-859e9c0c3a32",
          chapter_id: "English Intro Bab 2",
          score: 75,
          grade: 1,
        });

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
});

// Get One Report
const body = {
  student: {
    id: "3c5d6601-43b9-49e7-9ee1-406445d28823",
  },
};

const token = jwt.sign(body, process.env.JWT_SECRET, {
  expiresIn: "60d",
});

let tokenStudent;

describe("Get One Report Test", () => {
  describe("/report GET", () => {
    it("It should success", async () => {
      if (student.status == "student") {
        const res = await request(app)
          .get("/report")
          .send({
            report_id: "3c5d6601-43b9-49e7-9ee1-406445d28823",
          })
          .set({
            Authorization: `Bearer ${tokenStudent}`,
          });
        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      }
    });

    it("It should error report not found", async () => {
      if (student.status == "student") {
        const res = await request(app)
          .get("/report")
          .send({
            report_id: "English Advance Bab 1",
          })
          .set({
            Authorization: `Bearer ${tokenStudent}`,
          });
        expect(res.statusCode).toEqual(404);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Report Not Found");
      }
    });
  });
});

// Get all Report
describe("Get One Report Test", () => {
  describe("/report GET", () => {
    it("It should success", async () => {
      if (student.status == "student") {
        const res = await request(app)
          .get("/report/all")
          .set({
            Authorization: `Bearer ${tokenStudent}`,
          });
        expect(res.statusCode).toEqual(200);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      }
    });
  });
});
