const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcrypt");
const JWTstrategy = require("passport-jwt").Strategy;
const ExtractJWT = require("passport-jwt").ExtractJwt;
const { teacher } = require("../../db/models");

passport.serializeUser(function (teacher, done) {
  done(null, teacher.id);
});

passport.deserializeUser(function (id, done) {
  done(null, teacher.id);
});

module.exports.signup = (req, res, next) => {
  passport.authenticate("signupTeacher", (err, teacher, info) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }

    if (!teacher) {
      return res.status(401).json({
        message: info.message,
      });
    }

    req.teacher = teacher;

    next();
  })(req, res, next);
};

passport.use(
  "signupTeacher",
  new LocalStrategy(
    {
      usernameField: "username",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, username, password, done) => {
      try {
        let teacherSignup = await teacher.create(req.body);

        return done(null, teacherSignup, {
          message: "Teacher can be created",
        });
      } catch (e) {
        return done(null, false, {
          message: "Username has been used",
        });
      }
    }
  )
);

exports.signin = (req, res, next) => {
  passport.authenticate(
    "signinTeacher",
    { session: false },
    (err, teacher, info) => {
      if (err) {
        return res.status(500).json({
          message: "Internal Server Error",
          error: err.message,
        });
      }

      if (!teacher) {
        return res.status(401).json({
          message: info.message,
        });
      }
      req.teacher = teacher;

      next();
    }
  )(req, res, next);
};

passport.use(
  "signinTeacher",
  new LocalStrategy(
    {
      usernameField: "username",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, username, password, done) => {
      try {
        let teacherSignIn = await teacher.findOne({ where: { username } });

        if (!teacherSignIn) {
          return done(null, false, {
            message: "Username not found",
          });
        }

        let validate = await bcrypt.compare(password, teacherSignIn.password);

        if (!validate) {
          return done(null, false, {
            message: "Wrong password",
          });
        }
        return done(null, teacherSignIn, {
          message: "Teacher can sign in",
        });
      } catch (e) {
        return done(null, false, {
          message: "Teacher can't sign in",
        });
      }
    }
  )
);

exports.teacher = (req, res, next) => {
  passport.authorize("teacher", (err, teacher, info) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err.message,
      });
    }

    if (!teacher) {
      return res.status(403).json({
        message: info.message,
      });
    }

    req.teacher = teacher;

    next();
  })(req, res, next);
};

passport.use(
  "teacher",
  new JWTstrategy(
    {
      secretOrKey: process.env.JWT_SECRET,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        const teacherSignin = await teacher.findOne({
          where: { id: token.teacher.id },
        });

        if (teacherSignin.role.includes("teacher")) {
          return done(null, token.teacher);
        }

        return done(null, false, {
          message: "You're not authorized",
        });
      } catch (e) {
        return done(null, false, {
          message: "You're not authorized",
        });
      }
    }
  )
);
