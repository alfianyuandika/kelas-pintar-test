const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcrypt");
const JWTstrategy = require("passport-jwt").Strategy;
const ExtractJWT = require("passport-jwt").ExtractJwt;
const { student } = require("../../db/models");

passport.serializeUser(function (student, done) {
  done(null, student.id);
});

passport.deserializeUser(function (id, done) {
  done(null, student.id);
});

module.exports.signup = (req, res, next) => {
  passport.authenticate("signupStudent", (err, student, info) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }

    if (!student) {
      return res.status(401).json({
        message: info.message,
      });
    }

    req.student = student;

    next();
  })(req, res, next);
};

passport.use(
  "signupStudent",
  new LocalStrategy(
    {
      usernameField: "username",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, username, password, done) => {
      try {
        let studentSignup = await student.create(req.body);

        return done(null, studentSignup, {
          message: "student can be created",
        });
      } catch (e) {
        return done(null, false, {
          message: "Username has been used",
        });
      }
    }
  )
);

exports.signin = (req, res, next) => {
  passport.authenticate(
    "signinStudent",
    { session: false },
    (err, student, info) => {
      if (err) {
        return res.status(500).json({
          message: "Internal Server Error",
          error: err.message,
        });
      }

      if (!student) {
        return res.status(401).json({
          message: info.message,
        });
      }

      req.student = student;

      next();
    }
  )(req, res, next);
};

passport.use(
  "signinStudent",
  new LocalStrategy(
    {
      usernameField: "username",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, username, password, done) => {
      try {
        let studentSignIn = await student.findOne({ where: { username } });

        if (!studentSignIn) {
          return done(null, false, {
            message: "Username not found",
          });
        }

        let validate = await bcrypt.compare(password, studentSignIn.password);

        if (!validate) {
          return done(null, false, {
            message: "Wrong password",
          });
        }

        return done(null, studentSignIn, {
          message: "student can sign in",
        });
      } catch (e) {
        return done(null, false, {
          message: "student can't sign in",
        });
      }
    }
  )
);

exports.student = (req, res, next) => {
  passport.authorize("student", (err, student, info) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err.message,
      });
    }

    if (!student) {
      return res.status(403).json({
        message: info.message,
      });
    }

    req.student = student;

    next();
  })(req, res, next);
};

passport.use(
  "student",
  new JWTstrategy(
    {
      secretOrKey: process.env.JWT_SECRET,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        const studentSignin = await student.findOne({
          where: { id: token.student.id },
        });

        if (studentSignin.role.includes("student")) {
          return done(null, token.student);
        }

        return done(null, false, {
          message: "You're not authorized",
        });
      } catch (e) {
        return done(null, false, {
          message: "You're not authorized",
        });
      }
    }
  )
);
