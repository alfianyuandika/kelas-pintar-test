const validator = require("validator");

module.exports.signup = async (req, res, next) => {
  let errors = [];
  try {
    if (!validator.isStrongPassword(req.body.password)) {
      errors.push(
        "Password must have minimum length of 8 characters with minimum 1 lowercase character, 1 uppercase character, 1 number and 1 symbol"
      );
    }
    if (Object.keys(req.body).includes("confirmPassword")) {
      if (req.body.confirmPassword !== req.body.password) {
        errors.push("Password confirmation must be the same with password");
      }
    }
    if (req.body.grade < 0 || req.body.grade > 12) {
      errors.push("Grade must be between 0 - 12 ");
    }

    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", and "),
      });
    }

    next();
  } catch (err) {
    return res.status(500).json({
      message: err.message,
    });
  }
};

module.exports.signin = async (req, res, next) => {
  let errors = [];
  try {
    if (!validator.isStrongPassword(req.body.password)) {
      errors.push(
        "Password must have minimum length of 8 characters with minimum 1 lowercase character, 1 uppercase character, 1 number and 1 symbol"
      );
    }

    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", and "),
      });
    }

    next();
  } catch (err) {
    return res.status(500).json({
      message: err.message,
    });
  }
};
