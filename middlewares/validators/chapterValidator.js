const { subject, chapter } = require("../../db/models");
const validator = require("validator");

exports.create = async (req, res, next) => {
  try {
    let errors = [];

    let findData = await subject.findOne({
      where: { id: req.body.subject_id },
    });

    if (!findData) {
      return res.status(404).json({
        message: "Subject ID Not Found",
      });
    }
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};

exports.update = async (req, res, next) => {
  try {
    let findData = await Promise.all([
      subject.findOne({
        where: { id: req.body.new_subject_id },
      }),
      chapter.findOne({
        where: { id: req.body.chapter_id },
      }),
    ]);
    let errors = [];

    if (!findData[0]) {
      errors.push("Subject ID Not Found");
    }
    if (!findData[1]) {
      errors.push("Chapter ID Not Found");
    }

    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    let errors = [];

    let findData = await chapter.findOne({
      where: { id: req.body.chapter_id },
    });

    if (!findData) {
      return res.status(404).json({
        message: "Chapter ID Not Found",
      });
    }
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};
