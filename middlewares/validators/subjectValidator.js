const { subject } = require("../../db/models");
const validator = require("validator");

exports.create = async (req, res, next) => {
  try {
    let errors = [];

    if (!validator.isNumeric(req.body.grade)) {
      errors.push("Score must be a number");
    }
    if (req.body.grade < 1 || req.body.grade > 12) {
      errors.push("Grade must be between 1 - 12 ");
    }
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};

exports.update = async (req, res, next) => {
  try {
    let errors = [];

    let findData = await subject.findOne({
      where: { id: req.body.subject_id },
    });

    if (!findData) {
      return res.status(404).json({
        message: "Subject ID Not Found",
      });
    }
    if (req.body.new_grade < 1 || req.body.new_grade > 12) {
      errors.push("Grade must be between 1 - 12 ");
    }
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    let errors = [];

    let findData = await subject.findOne({
      where: { id: req.body.subject_id },
    });

    if (!findData) {
      return res.status(404).json({
        message: "Subject ID Not Found",
      });
    }
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};
