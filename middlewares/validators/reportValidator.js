const { student, subject, chapter, report } = require("../../db/models");

const validator = require("validator");

exports.create = async (req, res, next) => {
  try {
    let findData = await Promise.all([
      student.findOne({
        where: { id: req.body.student_id },
      }),
      chapter.findOne({
        where: { id: req.body.chapter_id },
      }),
    ]);

    let errors = [];
    if (!findData[0]) {
      errors.push("Student ID Not Found");
    }
    if (!findData[1]) {
      errors.push("Chapter ID Not Found");
    }
    if (!validator.isNumeric(req.body.score)) {
      errors.push("Score must be a number");
    }
    if (req.body.score < 0 || req.body.score > 100) {
      errors.push("Score must be between 0 - 100 ");
    }

    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
    req.body.subject_id = findData[1].subject_id;
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    let errors = [];

    let findData = await subject.findOne({
      where: { id: req.body.subject_id },
    });

    if (!findData) {
      return res.status(404).json({
        message: "Subject ID Not Found",
      });
    }
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};
