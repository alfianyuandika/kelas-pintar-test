const { student, subject, chapter, report } = require("../db/models");

student.hasMany(report, { foreignKey: "student_id" });
report.belongsTo(student, { foreignKey: "student_id" });

subject.hasMany(chapter, { foreignKey: "subject_id" });
chapter.belongsTo(subject, { foreignKey: "subject_id" });

student.hasMany(report, { foreignKey: "student_id" });
report.belongsTo(student, { foreignKey: "student_id" });

subject.hasMany(report, { foreignKey: "subject_id" });
report.belongsTo(subject, { foreignKey: "subject_id" });

chapter.hasMany(report, { foreignKey: "chapter_id" });
report.belongsTo(chapter, { foreignKey: "chapter_id" });
