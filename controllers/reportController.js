const path = require("path");
const Sequelize = require("sequelize");
const { QueryTypes } = require("sequelize");
const Op = Sequelize.Op;
const {
  student,
  subject,
  chapter,
  report,
  sequelize,
} = require("../db/models");

class ReportController {
  async createReport(req, res) {
    try {
      let createdData = await report.create({
        student_id: req.body.student_id,
        subject_id: req.body.subject_id,
        chapter_id: req.body.chapter_id,
        score: req.body.score,
        grade: req.body.grade,
      });

      let data = await report.findOne({
        where: { id: createdData.id },
        attributes: ["id", "score", "grade", ["createdAt", "createdAt"]],
        include: [
          {
            model: student,
            attributes: ["id", "name", "grade"],
          },
          {
            model: chapter,
            attributes: ["id", "chapter_name"],
            include: [
              {
                model: subject,
                attributes: ["id", "subject_name", "grade"],
              },
            ],
          },
        ],
      });
      return res.status(201).json({
        message: "Success Add to List",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getAllReport(req, res) {
    try {
      let data = await report.findAll({
        where: { student_id: req.student.id },
        attributes: ["id", "score"],
        include: [
          {
            model: student,
            attributes: ["id", "name", "grade"],
          },
          {
            model: chapter,
            attributes: ["id", "chapter_name"],
            include: [
              {
                model: subject,
                attributes: ["id", "subject_name", "grade"],
              },
            ],
          },
        ],
      });
      if (data.length == 0) {
        return res.status(400).json({
          message: "Report not found",
        });
      } else {
        return res.status(200).json({
          message: "Success",
          data,
        });
      }
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getOneReport(req, res) {
    try {
      let data = await report.findOne({
        where: { student_id: req.student.id, id: req.body.report_id },
        attributes: ["id", "score"],
        include: [
          {
            model: student,
            attributes: ["id", "name", "grade"],
          },
          {
            model: chapter,
            attributes: ["id", "chapter_name"],
            include: [
              {
                model: subject,
                attributes: ["id", "subject_name", "grade"],
              },
            ],
          },
        ],
      });
      if (data.length == 0) {
        return res.status(400).json({
          message: "Report not found",
        });
      } else {
        return res.status(200).json({
          message: "Success",
          data,
        });
      }
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getAverageScores(req, res) {
    try {
      let data = await sequelize.query(
        "WITH all_students AS (" +
          "SELECT student_id, grade, subject_id, AVG(score) AS avg_score " +
          "FROM kelaspintar_development.reports " +
          'WHERE subject_id = "' +
          req.body.subject_id +
          '" ' +
          "GROUP BY student_id, grade, subject_id " +
          ")," +
          "avg_grade AS ( " +
          "SELECT grade, AVG(avg_score) AS avg_grade_score " +
          "FROM all_students " +
          "GROUP BY grade " +
          " ) " +
          "SELECT all_students.*, avg_grade.avg_grade_score FROM all_students LEFT JOIN " +
          "avg_grade ON all_students.grade = avg_grade.grade ",
        { type: QueryTypes.SELECT }
      );

      if (data.length == 0) {
        return res.status(400).json({
          message: "Report not found",
        });
      } else {
        return res.status(200).json({
          message: "Success",
          data,
        });
      }
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getTopStudents(req, res) {
    try {
      let data = await sequelize.query(
        "WITH all_students AS (" +
          "SELECT student_id, grade, subject_id, AVG(score) AS avg_score " +
          "FROM kelaspintar_development.reports " +
          'WHERE subject_id = "' +
          req.body.subject_id +
          '" ' +
          "GROUP BY student_id, grade, subject_id " +
          ")," +
          "avg_grade AS ( " +
          "SELECT grade, AVG(avg_score) AS avg_grade_score " +
          "FROM all_students " +
          "GROUP BY grade " +
          " ) " +
          "SELECT als.*, avgg.avg_grade_score FROM all_students als " +
          "LEFT JOIN avg_grade avgg ON als.grade = avgg.grade " +
          "ORDER BY als.avg_score DESC " +
          "LIMIT 3",
        { type: QueryTypes.SELECT }
      );

      if (data.length == 0) {
        return res.status(400).json({
          message: "Report not found",
        });
      } else {
        return res.status(200).json({
          message: "Success",
          data,
        });
      }
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getBelowStudents(req, res) {
    try {
      let data = await sequelize.query(
        "WITH all_students AS (" +
          "SELECT student_id, grade, subject_id, AVG(score) AS avg_score " +
          "FROM kelaspintar_development.reports " +
          'WHERE subject_id = "' +
          req.body.subject_id +
          '" ' +
          "GROUP BY student_id, grade, subject_id " +
          ")," +
          "avg_grade AS ( " +
          "SELECT grade, AVG(avg_score) AS avg_grade_score " +
          "FROM all_students " +
          "GROUP BY grade " +
          " ) " +
          "SELECT als.*, avgg.avg_grade_score, std.name FROM all_students als " +
          "LEFT JOIN avg_grade avgg ON als.grade = avgg.grade " +
          "LEFT JOIN students std ON als.student_id = std.id " +
          "WHERE als.avg_score < avgg.avg_grade_score " +
          "ORDER BY als.avg_score DESC",
        { type: QueryTypes.SELECT }
      );

      if (data.length == 0) {
        return res.status(400).json({
          message: "Report not found",
        });
      } else {
        return res.status(200).json({
          message: "Success",
          data,
        });
      }
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new ReportController();
