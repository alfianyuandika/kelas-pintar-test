const { subject } = require("../db/models");

class SubjectController {
  async getAllSubject(req, res) {
    try {
      let data = await subject.findAll({
        attributes: ["id", "subject_name", "grade", ["createdAt", "createdAt"]],
      });

      if (data.length === 0) {
        return res.status(404).json({
          message: "Subject Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getOneSubject(req, res) {
    try {
      let data = await subject.findOne({
        where: { id: req.body.subject_id },
        attributes: ["id", "subject_name", "grade", ["createdAt", "createdAt"]],
      });

      if (!data) {
        return res.status(404).json({
          message: "Subject Not Found",
        });
      }

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async createSubject(req, res) {
    try {
      let data = await subject.create({
        id: req.body.id,
        subject_name: req.body.subject_name,
        grade: req.body.grade,
      });
      return res.status(201).json({
        message: "Create Subject Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async updateSubject(req, res) {
    let update = {
      id: req.body.new_subject_id,
      subject_name: req.body.new_subject_name,
      grade: req.body.new_grade,
    };
    try {
      let data = await subject.update(update, {
        where: { id: req.body.subject_id },
      });

      let updatedData = await subject.findOne({
        where: { id: req.body.new_subject_id },
        attributes: ["id", "subject_name", "grade", ["createdAt", "waktu"]],
      });
      return res.status(201).json({
        message: "Update Subject Success",
        updatedData,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async deleteSubject(req, res) {
    try {
      let data = await subject.destroy({
        where: { id: req.body.subject_id },
      });

      return res.status(201).json({
        message: "Delete Subject Success",
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new SubjectController();
