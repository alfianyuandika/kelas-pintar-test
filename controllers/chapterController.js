const { chapter, subject } = require("../db/models");

class ChapterController {
  async createChapter(req, res) {
    try {
      let createdData = await chapter.create({
        id: req.body.chapter_id,
        chapter_name: req.body.chapter_name,
        subject_id: req.body.subject_id,
      });

      let data = await chapter.findOne({
        where: { id: createdData.id },
        attributes: ["id", "chapter_name", ["createdAt", "createdAt"]],
        include: [
          {
            model: subject,
            attributes: ["id", "subject_name", "grade"],
          },
        ],
      });

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async deleteChapter(req, res) {
    try {
      let data = await chapter.destroy({
        where: {
          id: req.body.chapter_id,
        },
      });

      return res.status(200).json({
        message: "Success",
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getAllChapter(req, res) {
    try {
      let data = await chapter.findAll({
        attributes: ["id", "chapter_name"],
        include: [
          {
            model: subject,
            attributes: ["id", "subject_name", "grade"],
          },
        ],
      });

      if (data.length == 0) {
        return res.status(400).json({
          message: "Chapter not found",
        });
      } else {
        return res.status(200).json({
          message: "Success",
          data,
        });
      }
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getOneChapter(req, res) {
    try {
      let data = await chapter.findOne({
        where: { id: req.body.chapter_id },
        attributes: ["id", "chapter_name"],
        include: [
          {
            model: subject,
            attributes: ["id", "subject_name", "grade"],
          },
        ],
      });

      if (!data) {
        return res.status(400).json({
          message: "Chapter not found",
        });
      } else {
        return res.status(200).json({
          message: "Success",
          data,
        });
      }
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async updateChapter(req, res) {
    let update = {
      id: req.body.new_chapter_id,
      chapter_name: req.body.new_chapter_name,
      subject_id: req.body.new_subject_id,
    };
    try {
      let data = await chapter.update(update, {
        where: { id: req.body.chapter_id },
      });

      let updatedData = await chapter.findOne({
        where: { id: req.body.new_chapter_id },
        attributes: ["id", "chapter_name"],
        include: [
          {
            model: subject,
            attributes: ["id", "subject_name", "grade"],
          },
        ],
      });

      return res.status(201).json({
        message: "Update Category Success",
        updatedData,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new ChapterController();
