const jwt = require("jsonwebtoken");

class TeacherAuthController {
  async getToken(req, res) {
    try {
      const body = {
        teacher: {
          id: req.teacher.id,
          name: req.teacher.name,
          username: req.teacher.username,
        },
      };

      const token = jwt.sign(body, process.env.JWT_SECRET, {
        expiresIn: "60d",
      });
      const id = body.teacher.id;
      const name = body.teacher.name;
      const username = body.teacher.username;

      return res.status(200).json({
        message: "Success",
        id,
        name,
        username,
        token,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }
}

module.exports = new TeacherAuthController();
