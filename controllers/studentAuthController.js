const jwt = require("jsonwebtoken");

class StudentAuthController {
  async getToken(req, res) {
    try {
      const body = {
        student: {
          id: req.student.id,
          name: req.student.name,
          username: req.student.username,
          grade: req.student.grade
        },
      };

      const token = jwt.sign(body, process.env.JWT_SECRET, {
        expiresIn: "60d",
      });
      const id = body.student.id;
      const name = body.student.name;
      const username = body.student.username;
      const grade = body.student.grade;

      return res.status(200).json({
        message: "Success",
        id,
        name,
        username,
        grade,
        token,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }
}

module.exports = new StudentAuthController();
