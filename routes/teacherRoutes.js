const express = require("express");
const router = express.Router();

const teacherAuthValidator = require("../middlewares/validators/teacherAuthValidator");

const teacherAuth = require("../middlewares/auth/teacherAuth");

const teacherAuthController = require("../controllers/teacherAuthController");

router.post(
  "/signup",
  teacherAuthValidator.signup,
  teacherAuth.signup,
  teacherAuthController.getToken
);

router.post(
  "/signin",
  teacherAuthValidator.signin,
  teacherAuth.signin,
  teacherAuthController.getToken
);

module.exports = router;
