const express = require("express");
const router = express.Router();

const subjectValidator = require("../middlewares/validators/subjectValidator");

const subjectController = require("../controllers/subjectController");

const teacherAuth = require("../middlewares/auth/teacherAuth");

router.get("/all", subjectController.getAllSubject);
router.get("/", subjectController.getOneSubject);
router.post(
  "/teacher/create",
  teacherAuth.teacher,
  subjectValidator.create,
  subjectController.createSubject
);
router.patch(
  "/teacher/update",
  teacherAuth.teacher,
  subjectValidator.update,
  subjectController.updateSubject
);
router.delete(
  "/teacher/delete",
  teacherAuth.teacher,
  subjectValidator.delete,
  subjectController.deleteSubject
);

module.exports = router;
