const express = require("express");
const router = express.Router();

const studentAuthValidator = require("../middlewares/validators/studentAuthValidator");

const studentAuth = require("../middlewares/auth/studentAuth");

const studentAuthController = require("../controllers/studentAuthController");

router.post(
  "/signup",
  studentAuthValidator.signup,
  studentAuth.signup,
  studentAuthController.getToken
);

router.post(
  "/signin",
  studentAuthValidator.signin,
  studentAuth.signin,
  studentAuthController.getToken
);

module.exports = router;
