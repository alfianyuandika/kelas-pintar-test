const express = require("express");
const router = express.Router();

const reportValidator = require("../middlewares/validators/reportValidator");

const reportController = require("../controllers/reportController");

const studentAuth = require("../middlewares/auth/studentAuth");
const teacherAuth = require("../middlewares/auth/teacherAuth");


router.get("/mvp", studentAuth.student, reportController.getTopStudents);
router.get("/low", studentAuth.student, reportController.getBelowStudents);
router.get("/all", studentAuth.student, reportController.getAllReport);
router.get("/", studentAuth.student, reportController.getOneReport);

router.get(
  "/student/avgscr",
  studentAuth.student,
  reportController.getAverageScores
);

router.post(
  "/teacher/create",
  teacherAuth.teacher,
  reportValidator.create,
  reportController.createReport
);

module.exports = router;
