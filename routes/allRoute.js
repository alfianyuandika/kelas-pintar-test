const express = require("express");
const router = express.Router();

// Import All routes
const student = require("./studentRoutes");
const teacher = require("./teacherRoutes");
const subject = require("./subjectRoutes")
const chapter = require("./chapterRoutes")
const report = require("./reportRoutes")

// Use routes
router.use("/student", student)
router.use("/teacher", teacher)
router.use("/subject", subject)
router.use("/chapter", chapter)
router.use("/report", report)


module.exports = router;
