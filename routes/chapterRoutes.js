const express = require("express");
const router = express.Router();

const chapterValidator = require("../middlewares/validators/chapterValidator");

const chapterController = require("../controllers/chapterController");

const teacherAuth = require("../middlewares/auth/teacherAuth");

router.get("/all", chapterController.getAllChapter);

router.get("/", chapterController.getOneChapter);
router.post(
  "/teacher/create",
  teacherAuth.teacher,
  chapterValidator.create,
  chapterController.createChapter
);
router.patch(
  "/teacher/update",
  teacherAuth.teacher,
  chapterValidator.update,
  chapterController.updateChapter
);
router.delete(
  "/teacher/delete",
  teacherAuth.teacher,
  chapterValidator.delete,
  chapterController.deleteChapter
);

module.exports = router;
