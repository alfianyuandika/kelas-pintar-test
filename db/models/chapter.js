"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class chapter extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  chapter.init(
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      chapter_name: DataTypes.STRING,
      grade: DataTypes.INTEGER,
      subject_id: DataTypes.STRING,
    },
    {
      sequelize,
      paranoid: true,
      timestamps: true,
      modelName: "chapter",
    }
  );
  return chapter;
};
