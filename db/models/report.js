"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class report extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  report.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      student_id: DataTypes.UUID,
      grade: DataTypes.INTEGER,
      subject_id: DataTypes.STRING,
      chapter_id: DataTypes.STRING,
      score: DataTypes.INTEGER,
    },
    {
      sequelize,
      paranoid: true,
      timestamps: true,
      modelName: "report",
    }
  );
  return report;
};
