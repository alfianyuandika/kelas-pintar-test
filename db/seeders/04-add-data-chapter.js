"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("chapters", [
      {
        id: "Math Intro Bab 1",
        chapter_name: "Math Introduction 1",
        grade: 1,
        subject_id: "Math 101",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Math intro Bab 2",
        chapter_name: "Math Introduction 2",
        grade: 1,
        subject_id: "Math 101",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Math Intro Bab 3",
        chapter_name: "Math Introduction 3",
        grade: 1,
        subject_id: "Math 101",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Math Advance Bab 1",
        chapter_name: "Math Advance 1",
        grade: 2,
        subject_id: "Math 201",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Math Advance Bab 2",
        chapter_name: "Math Advance 2",
        grade: 2,
        subject_id: "Math 201",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Math Advance Bab 3",
        chapter_name: "Math Advance 3",
        grade: 2,
        subject_id: "Math 201",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Math Expert Bab 1",
        chapter_name: "Math Expert 1",
        grade: 3,
        subject_id: "Math 301",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Math Expert Bab 2",
        chapter_name: "Math Expert 2",
        grade: 3,
        subject_id: "Math 301",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Math Expert Bab 3",
        chapter_name: "Math Expert 3",
        grade: 3,
        subject_id: "Math 301",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "English Intro Bab 1",
        chapter_name: "English Introduction 1",
        grade: 1,
        subject_id: "English 101",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "English Intro Bab 2",
        chapter_name: "English Introduction 2",
        grade: 1,
        subject_id: "English 101",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "English Intro Bab 3",
        chapter_name: "English Introduction 3",
        grade: 1,
        subject_id: "English 101",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "English Advance Bab 1",
        chapter_name: "English Advance 1",
        grade: 2,
        subject_id: "English 201",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "English Advance Bab 2",
        chapter_name: "English Advance 2",
        grade: 2,
        subject_id: "English 201",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "English Advance Bab 3",
        chapter_name: "English Advance 3",
        grade: 2,
        subject_id: "English 201",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "English Expert Bab 1",
        chapter_name: "English Expert 1",
        grade: 3,
        subject_id: "English 301",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "English Expert Bab 2",
        chapter_name: "English Expert 2",
        grade: 3,
        subject_id: "English 301",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "English Expert Bab 3",
        chapter_name: "English Expert 3",
        grade: 3,
        subject_id: "English 301",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Indonesian Intro Bab 1",
        chapter_name: "Indonesian Introduction 1",
        grade: 1,
        subject_id: "Indonesian 101",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Indonesian Intro Bab 2",
        chapter_name: "Indonesian Introduction 2",
        grade: 1,
        subject_id: "Indonesian 101",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Indonesian Intro Bab 3",
        chapter_name: "Indonesian Introduction 3",
        grade: 1,
        subject_id: "Indonesian 101",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Indonesian Advance Bab 1",
        chapter_name: "Indonesian Advance 1",
        grade: 2,
        subject_id: "Indonesian 201",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Indonesian Advance Bab 2",
        chapter_name: "Indonesian Advance 2",
        grade: 2,
        subject_id: "Indonesian 201",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Indonesian Advance Bab 3",
        chapter_name: "Indonesian Advance 3",
        grade: 2,
        subject_id: "Indonesian 201",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Indonesian Expert Bab 1",
        chapter_name: "Indonesian Expert 1",
        grade: 3,
        subject_id: "Indonesian 301",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Indonesian Expert Bab 2",
        chapter_name: "Indonesian Expert 2",
        grade: 3,
        subject_id: "Indonesian 301",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Indonesian Expert Bab 3",
        chapter_name: "Indonesian Expert 3",
        grade: 3,
        subject_id: "Indonesian 301",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Science Intro Bab 1",
        chapter_name: "Science Introduction 1",
        grade: 1,
        subject_id: "Science 101",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Science Intro Bab 2",
        chapter_name: "Science Introduction 2",
        grade: 1,
        subject_id: "Science 101",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Science Intro Bab 3",
        chapter_name: "Science Introduction 3",
        grade: 1,
        subject_id: "Science 101",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Science Advance Bab 1",
        chapter_name: "Science Advance 1",
        grade: 2,
        subject_id: "Science 201",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Science Advance Bab 2",
        chapter_name: "Science Advance 2",
        grade: 2,
        subject_id: "Science 201",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Science Advance Bab 3",
        chapter_name: "Science Advance 3",
        grade: 2,
        subject_id: "Science 201",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Science Expert Bab 1",
        chapter_name: "Science Expert 1",
        grade: 3,
        subject_id: "Science 301",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Science Expert Bab 2",
        chapter_name: "Science Expert 2",
        grade: 3,
        subject_id: "Science 301",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Science Expert Bab 3",
        chapter_name: "Science Expert 3",
        grade: 3,
        subject_id: "Science 301",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
