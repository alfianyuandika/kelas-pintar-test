"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("students", [
      {
        id: "452f6aae-6688-42c3-b576-859e9c0c3a32",
        name: "Student1",
        username: "student1",
        grade: 1,
        password: "$2b$10$NRWso4JS6eo4ltbOY9p9n.PcVJl1U4We9QgK98OTAGGRgMIb4CCue",
        role: "student",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "452f6aae-6688-42c3-b576-859e9c0c3a33",
        name: "Student2",
        username: "student2",
        grade: 2,
        password: "$2b$10$NRWso4JS6eo4ltbOY9p9n.PcVJl1U4We9QgK98OTAGGRgMIb4CCue",
        role: "student",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "452f6aae-6688-42c3-b576-859e9c0c3a34",
        name: "Student3",
        username: "student3",
        grade: 3,
        password: "$2b$10$NRWso4JS6eo4ltbOY9p9n.PcVJl1U4We9QgK98OTAGGRgMIb4CCue",
        role: "student",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "452f6aae-6688-42c3-b576-859e9c0c3a35",
        name: "Student4",
        username: "student4",
        grade: 4,
        password: "$2b$10$NRWso4JS6eo4ltbOY9p9n.PcVJl1U4We9QgK98OTAGGRgMIb4CCue",
        role: "student",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "452f6aae-6688-42c3-b576-859e9c0c3a36",
        name: "Student5",
        username: "student5",
        grade: 1,
        password: "$2b$10$NRWso4JS6eo4ltbOY9p9n.PcVJl1U4We9QgK98OTAGGRgMIb4CCue",
        role: "student",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "452f6aae-6688-42c3-b576-859e9c0c3a37",
        name: "Student6",
        username: "student6",
        grade: 2,
        password: "$2b$10$NRWso4JS6eo4ltbOY9p9n.PcVJl1U4We9QgK98OTAGGRgMIb4CCue",
        role: "student",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "452f6aae-6688-42c3-b576-859e9c0c3a38",
        name: "Student7",
        username: "student7",
        grade: 3,
        password: "$2b$10$NRWso4JS6eo4ltbOY9p9n.PcVJl1U4We9QgK98OTAGGRgMIb4CCue",
        role: "student",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "452f6aae-6688-42c3-b576-859e9c0c3a39",
        name: "Student8",
        username: "student8",
        grade: 4,
        password: "$2b$10$NRWso4JS6eo4ltbOY9p9n.PcVJl1U4We9QgK98OTAGGRgMIb4CCue",
        role: "student",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
