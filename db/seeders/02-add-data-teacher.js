"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("teachers", [
      {
        id: "452f6aae-6688-42c3-b576-859e9c0c3a35",
        name: "Teacher1",
        username: "teacher1",
        password: "Teacher1_123",
        role: "teacher",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "452f6aae-6688-42c3-b576-859e9c0c3a36",
        name: "Teacher2",
        username: "teacher2",
        password: "Teacher2_123",
        role: "teacher",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "452f6aae-6688-42c3-b576-859e9c0c3a37",
        name: "Teacher3",
        username: "teacher3",
        password: "Teacher3_123",
        role: "teacher",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
