"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("subjects", [
      {
        id: "Math 101",
        subject_name: "Math",
        grade: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Math 201",
        subject_name: "Math",
        grade: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Math 301",
        subject_name: "Math",
        grade: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "English 101",
        subject_name: "English",
        grade: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "English 201",
        subject_name: "English",
        grade: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "English 301",
        subject_name: "English",
        grade: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Indonesian 101",
        subject_name: "Indonesian",
        grade: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Indonesian 201",
        subject_name: "Indonesian",
        grade: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Indonesian 301",
        subject_name: "Indonesian",
        grade: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Science 101",
        subject_name: "Science",
        grade: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Science 201",
        subject_name: "Science",
        grade: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "Science 301",
        subject_name: "Science",
        grade: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
