"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("reports", {
      id: {
        allowNull: false,
        primaryKey: true,
        unique: true,
        type: Sequelize.UUID,
      },
      student_id: {
        allowNull: false,
        type: Sequelize.UUID,
      },
      grade: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      subject_id: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      chapter_id: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      score: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addConstraint("reports", {
      fields: ["student_id"],
      type: "foreign key",
      name: "fkey_student_report",
      references: {
        table: "students",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.addConstraint("reports", {
      fields: ["subject_id"],
      type: "foreign key",
      name: "fkey_subject_report",
      references: {
        table: "subjects",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.addConstraint("reports", {
      fields: ["chapter_id"],
      type: "foreign key",
      name: "fkey_chapter_report",
      references: {
        table: "chapters",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("reports");
  },
};
