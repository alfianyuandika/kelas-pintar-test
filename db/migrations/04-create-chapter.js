"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("chapters", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      chapter_name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      grade: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      subject_id: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addConstraint("chapters", {
      fields: ["subject_id"],
      type: "foreign key",
      name: "fkey_subject_chapter",
      references: {
        //Required field
        table: "subjects",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("chapters");
  },
};
